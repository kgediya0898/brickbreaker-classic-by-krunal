var game = new Phaser.Game(800,600,Phaser.CANVAS,'gameDiv');
var helperInteger = 0;
var buffTimeOutSeconds=0;
var defender;
var ball;

var bricks;
var tutorialText;
var newBrick;
var brickInfo;
var buff;
var logoTimeOutSeconds=0;
var speed=8;
var scoreText;
var textbuff;
var score =0;
var Speedflag=0;
var Sizeflag=0;
var BallSpeed=0;
var logo;
var gameStartFlag=1;
var brickBreak;
var buffAcquired;
var lobbyNumberText;
var bounce;
var lobbyNumber=1;
var mainState = {


preload:function(){

//Scaling Canvas to Centre Of The Window
game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
game.scale.pageAlignHorizontally = true;
game.scale.pageAlignVertically = true;

//Setting Background Color
game.stage.backgroundColor = '#fbb750';

//Loading Assets
game.load.audio('bounce',"assets/sounds/Bounce.mp3");
game.load.audio('brickBreak',"assets/sounds/BrickBreak.mp3");
game.load.audio('buffAcquired',"assets/sounds/buffAcquired.mp3");
game.load.image('logo',"assets/sprites/dandc_logo.jpg");
game.load.image('brick',"assets/sprites/Brick.svg");
game.load.image('ball1',"assets/sprites/ball.svg");
game.load.image('paddle',"assets/sprites/player-bar.svg");
game.load.image('brickYellow',"assets/sprites/brick_yellow.png");
game.load.spritesheet('bricks', 'assets/sprites/bricks.png', 30, 15);

	  
	},


	create:function(){
/*Initializing Buff Timeout Seconds
Setting it -1 so buffRemoved Function is not called before addingBuff
*/
buffTimeOutSeconds=-1;

//Adding Sounds

buffAcquired=game.add.audio('buffAcquired');
bounce=game.add.audio('bounce');
brickBreak=game.add.audio('brickBreak');
//Adding Texts
lobbyNumberText=this.game.add.text(game.world.width-300,10,"Lobby Number : "+lobbyNumber);
tutorialText=this.game.add.text(game.world.width*0.5-100, game.world.height*0.5,"Press UP to START");
scoreText = this.game.add.text(20,10,"Score : "+score);

//Starting Physics Engine
game.physics.startSystem(Phaser.Physics.ARCADE);

//Defining position of ball and defender

var posXDefender=300;
var posXBall=posXDefender-40;
//Adding Ball And Initializing it's Properties 	
 ball = game.add.sprite(game.world.width*0.5,posXBall, 'ball1');
 ball.scale.setTo(0.1,0.1);
    game.physics.enable(ball, Phaser.Physics.ARCADE);
    ball.body.velocity.set(0, 0);
    ball.body.bounce.y=1;
    ball.body.bounce.x=1;
    ball.anchor.set(0.5);
    ball.body.collideWorldBounds="true";

//Setting logo timeout to 2 seconds
logoTimeOutSeconds=Math.floor(game.time.now/1000);
logoTimeOutSeconds+=2;

//Adding Paddle(defender) And Initializing it's Properties
defender = game.add.sprite(game.world.width*0.5,posXDefender,'paddle');
defender.scale.setTo(0.5,.5);
game.physics.enable(defender, Phaser.Physics.ARCADE);
defender.body.collideWorldBounds="true";
defender.anchor.set(0.5,1);

//Setting immovable true so that on colliding with ball it doesn't move due to recoil (Collision Effect)
defender.body.immovable="true";



//Calling CreateBricks to Setup Bricks
createBricks();

//Adding logo as topmost layer
logo=game.add.sprite(0,0,'logo');


//Initiating keyboard keys
rightKey = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
leftKey = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);

//Disabling Collision with lower bounds
game.physics.arcade.checkCollision.up = false;
ball.checkWorldBounds = true;


//Setting GameOver Logic if it crosses lower bounds


ball.events.onOutOfBounds.add(function(){
	var secs=Math.floor(game.time.now/1000);
	var minute="seconds";
	if(secs>60){
		secs=secs/60;
		minute="minutes";
	}
	else{

	}
	Client.ballOutOfBounds(ball.position.x,ball.body.velocity.x,ball.body.velocity.y);
	/*Store the positon and velocity
	var ballPositonX =ball.position.x;
	var ballPositonY =ball.position.y;
	var ballVelocityX =ball.body.velocity.x;
	var ballVelocityY =ball.body.velocity.y;
	*/ //Above thing should be forwared to Server


   //alert('Game over! Your Score Was '+score+ " And you played for " + secs +" "+minute+ "  Ball position and velocity is "+ball.position+","+ball.body.velocity);

//Flip the positions and change direction of velocity which we recieved from  the server
/*ball.position.x=game.world.width - ballPositonX;
ball.position.y=ballPositonY;
ball.body.velocity.x=-ballVelocityX;
ball.body.velocity.y=-ballVelocityY;
*/
 
    //location.reload();
}, this);



    

   

//Setting upward gravity to 0 so effect of Gravity is discarded
game.physics.arcade.gravity.y = 0;
	
	},

	update:function(){


//Killing Logo after logoTimeOutSeconds
if(Math.floor(game.time.now/1000)==logoTimeOutSeconds){
		logo.kill();
	}

//Removing buffEffect after buffTimeOutSeconds	
if(Math.floor(game.time.now/1000)==buffTimeOutSeconds){
			buffRemove();
		}
			

//Setting Collision Logic		
//game.physics.arcade.collide(defender,buff,buffAdded);//read as If defender Collides with buff perform buffAdded() function
game.physics.arcade.collide(ball,defender,bounceDefender);//read as ball and defender are Collidable
 game.physics.arcade.collide(ball,bricks,killBrick);

//When game is first Started if we press UP launch ball
if(upKey.isDown&&ball.position.x==defender.position.x&&ball.position.y==defender.position.y-40&&gameStartFlag==1){
		 	tutorialText.setText("");
		 	ball.body.velocity.set(30,-300);
		 	gameStartFlag=0;
		 }
//Defender Movement Logic
if(rightKey.isDown){
if(gameStartFlag==1){
console.log(defender.position.y-20,defender.position.x,ball.position.y,ball.position.x); defender.x+=speed;
ball.position.x=defender.position.x;}else{


			defender.x +=speed;}
		}
		if(leftKey.isDown){
			if(gameStartFlag==1){ defender.x-=speed;
ball.position.x=defender.position.x;}else{
			defender.x-=speed;}
		}
		
	},


}
function bounceDefender(){
	bounce.play();
}
function addLobby(ied){
	id+=ied;

}
function createBricks(){

	//brickInfo Structure
	brickInfo = {
        width: 50,
        height: 20,
        count: {
            col: game.world.width/60,
            row: 5
        },
        offset: {
            top: 400,
            left:40
        },
        padding: 10
    }

    //Adding bricks as a group
    bricks = game.add.group();

    //Using Matrix Logic For Creating Bricks Pattern
var brickId=0;
    for(c=0; c<brickInfo.count.row; c++) {
        for(r=0; r<brickInfo.count.col-1; r++) {
        	brickId++;
            var brickX = (r*(brickInfo.width+brickInfo.padding))+brickInfo.offset.left;
            var brickY = (c*(brickInfo.height+brickInfo.padding))+brickInfo.offset.top;
            newBrick = game.add.sprite(brickX, brickY, 'brick');
            newBrick.scale.setTo(0.15,0.15);
            game.physics.enable(newBrick, Phaser.Physics.ARCADE);
            newBrick.body.immovable = true;
            newBrick.anchor.set(0.5);
            bricks.add(newBrick);
        }
    }
}
function buffAdded(defender,buff){
buffAcquired.play();
//Killing buff once it has been acquired by Defender	
buff.kill();

//Setting buffTimeOutSeconds to 5secs after current time
buffTimeOutSeconds=Math.floor((game.time.now/1000));
buffTimeOutSeconds+=5;

//Randomizing buffs
helperInteger=randomEvenInteger(2,5);

switch(helperInteger){
case 2:
Sizeflag=1;
console.log("Defender extended");
defender.scale.setTo(1.2,0.5);
break;

case 4:
console.log("Defender Speed Increased");
speed=12;
Speedflag=1;
break;

case 6:
console.log("Ball Speed Slowed Down");
BallSpeed=1;
ball.body.velocity.x= (ball.body.velocity.x*0.5);
ball.body.velocity.y=(ball.body.velocity.y*0.5);
break;
/*Add More Cases here for More Buffs*/
}
}
function buffRemove(){
	
if(Speedflag==1){
speed=8;
console.log("Speed Restored");
Speedflag=0;
}

if(Sizeflag==1){
defender.scale.setTo(0.5,0.5);
console.log("Defender size reverted to original");
Sizeflag=0;
}

if(BallSpeed==1){
ball.body.velocity.x= Math.sign(ball.body.velocity.x)*200;
ball.body.velocity.y= Math.sign(ball.body.velocity.y)*200;
console.log("Ball Speed reverted");
BallSpeed=0;
}
	
	/*Add more if statements for removing any additional buffs*/
	

	
}
function lobbyNumber(id) {
	lobbyNumberText.setText("Lobby Number : "+id);
}


function killBrick(ball,brick){
	brickBreak.play();
	//scoreManagemnet
	score+=1;
	scoreText.setText("Score : "+score);

	//Increasing VelocityMultiplyingFactor after each bounce

	//ball.body.bounce.x+=0.009;
	//ball.body.bounce.y+=0.009;

	//ball.body.bounce.x+=0.002;
//	ball.body.bounce.y+=0.002;

	

//Increase range here to decrease probability of dropping buffs
helperInteger =randomEvenInteger(3,5);


//Self Explaining Statements After This Point
if(helperInteger==4){
addBuff(brick);

}
	brick.kill();

}

function addBuff(brick){
buff = game.add.sprite(brick.position.x,brick.position.y,'brick');
game.physics.enable(buff,Phaser.Physics.ARCADE);
buff.body.velocity.set(0,200);
buff.scale.setTo(0.2,0.2);

}
function addBall(i,v,w){
	console.log("function called"+i+","+v+","+w);
	ball.position.x=game.world.width-i;
	ball.position.y+=1;
	ball.body.velocity.x=v;
	ball.body.velocity.y=w;
}

function randomEvenInteger(min,max){
	var rand = Math.floor(Math.random()*(max-min+1))+min;
	if(rand%2!=0){
		return rand+1;
	}
	else{
		return rand;
	}}

function randomfloat(min,max){
	var rand = Math.random()*(max-min+1)+min;
		return rand;
	
}
game.state.add('mainState',mainState);

game.state.start('mainState');